import React, { useState, Fragment } from 'react';

const About = ()=> {
  const[about,setAbout] = useState("About React")
  return(
    <div>
      <p>{about}</p>
    </div>
  )
}

export default About;