import React, { useState } from 'react';
import About from '../views/About';
import Home from '../views/Home';
import Example from '../views/Example';


const AboutPage = ()=> {
  return(
    <About/>
  )
}

const HomePage = ()=> {
  return(
    <Home/>
  )
}

const ExamplePage = ()=> {
  return(
    <Example/>
  )
}
export {HomePage, AboutPage, ExamplePage};