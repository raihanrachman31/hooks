import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import {HomePage, AboutPage, ExamplePage} from './routes/Routes'
import Header from './layout/Header'
import Footer from './layout/Footer'
import {Route} from 'react-router-dom';

const App = ()=> {
  const [name, setName] = useState("raihan")
  const [number,setNumber] = useState(1)
  const [player,setPlayer] = useState({
    name: "maldono",
    club: "Persiba"
  })
  const [fruits, setFruit] = useState (["apple","baaanan"])
  return(
    
    <div className="App">
      <Header/>
      <h1>helowold of</h1>
      <h2>{name}</h2>
      <h2>{number}</h2>
      <h3>{player.name}</h3>
      <h4>{fruits.map( fruit => (
        <div>
          <p>fruit name: {fruit}</p>
        </div>
      ))}</h4>
        <Route path ="/home" component= {HomePage}  exact/>
        <Route path ="/example" component= {ExamplePage}  exact/>
        <Route path ="/about" component= {AboutPage}  exact/>
      <Footer/>
    </div>
   
  )
}

export default App;
