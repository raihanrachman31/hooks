import React, { useState } from 'react';

import { Link } from "react-router-dom"

const Header = ()=> {
  return(
    <div>
        <Link to="/home" >home</Link>
        <Link to="/about" >about</Link>
        <Link to="/example" >example</Link>
    </div>
  )
}

export default Header;